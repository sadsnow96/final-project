package com.selenium.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import static org.testng.Assert.fail;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;

public class parallel {
	WebDriver driver;

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;

		}

	}

	public WebElement waitAndGetElement(By by, long timeout) throws InterruptedException {
		for (int i = 0; i < timeout / 500; i++) {
			if (isElementPresent(by)) {
				return driver.findElement(by);
			} else {
				System.out.println("==Element is not available");
				Thread.sleep(500);

			}

		}
		return null;
	}

	public void waitForElement2Disappear(By by, long timeout) throws InterruptedException {
		for (int i = 0; i < timeout / 500; i++) {
			if (isElementPresent(by)) {
				Thread.sleep(500);
				System.out.println("== Element is still available");
			} else {

				System.out.println("==Element is disappeared");
				return;
			}

		}

	}

	@BeforeMethod
	@Parameters("browser")
	public void beforeMethod(String browser) {
		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C://Users//LQA//Downloads//geckodriver.exe");
			driver = new FirefoxDriver();
			System.out.println("Run FF");
		} else {
			System.setProperty("webdriver.chrome.driver", "C://Users//LQA//Downloads//chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("Run Chrome");
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.get("http://automationpractice.com");
	}

	@Test(priority=1)
	public void testPro1() {
		driver.get("http://automationpractice.com");

		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();
		String email = "TunNgn";
		driver.findElement(By.xpath("//*[@id=\"email_create\"]")).sendKeys(email);
		driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]/span")).click();

		By locator = By.xpath("//*[@id=\"create_account_error\"]");
		if (isElementPresent(locator)) {
			System.out.println("bai test thanh cong");
		} else {
			fail("test fail");

		}

//		WebDriverWait wait = new WebDriverWait(driver, 5);
//		WebElement compare = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Invalid')]")));
//	
//		if()
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	@Test(priority = 2)
	public void testPro2() throws InterruptedException {
		driver.get("http://automationpractice.com");
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();

		// khai bao profile
		String FirstName = "Tuan";
		String Address = "HaNoi";
		String LastName = "Nguyen";
		String ZipCode = "10000";
		String PhoneNo = "123451";
		String email = "1eq21211uan4works250298@gmail.com";
		String pw = "12345";

		// sendKeys email and click button
		driver.findElement(By.xpath("//*[@id=\"email_create\"]")).sendKeys(email);
		driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]/span")).click();

		Thread.sleep(3000);

		// sendKeys information
		driver.findElement(By.xpath("//*[@id=\"id_gender1\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"customer_firstname\"]")).sendKeys(FirstName);
		driver.findElement(By.xpath("//*[@id=\"customer_lastname\"]")).sendKeys(LastName);

		// check email present or not then sendKeys if not
		String emailText = driver.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("value");
		if (!email.equals(emailText)) {
			driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(email);
		} else {
			System.out.println("email da duoc dien tu dong la " + email);
		}

		// sendKeys information
		driver.findElement(By.xpath("//*[@id=\"passwd\"]")).sendKeys(pw);

		// select day/month/year
		Select selectdays = new Select(driver.findElement(By.xpath("//*[@name=\"days\"]")));
		selectdays.selectByIndex(25);
		Select selectmonths = new Select(driver.findElement(By.xpath("//*[@name=\"months\"]")));
		selectmonths.selectByIndex(2);
		Select selectyears = new Select(driver.findElement(By.xpath("//*[@name=\"years\"]")));
		selectyears.selectByValue("1998");

		// tick to checkbox
		driver.findElement(By.xpath("//*[@id=\"newsletter\"]")).click();

		// check in4
		String LastText = driver.findElement(By.xpath("//*[@id=\"lastname\"]")).getAttribute("value");
		String FirstText = driver.findElement(By.xpath("//*[@id=\"firstname\"]")).getAttribute("value");

		if (!FirstText.equals(FirstName)) {
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys(FirstName);
		} else {
			System.out.println("First Name da duoc dien tu dong la " + FirstName);
		}

		if (!LastText.equals(LastName)) {
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys(LastName);
		} else {
			System.out.println("Last Name da duoc dien tu dong la " + LastName);
		}

		// sendKeys in4mation
		driver.findElement(By.xpath("//*[@id=\"address1\"]")).sendKeys(Address);
		driver.findElement(By.xpath("//*[@id=\"city\"]")).sendKeys(Address);
		Select selectstates = new Select(driver.findElement(By.xpath("//*[@id=\"id_state\"]")));
		selectstates.selectByVisibleText("Texas");

		driver.findElement(By.xpath("//*[@id=\"postcode\"]")).sendKeys(ZipCode);

		driver.findElement(By.xpath("//*[@id=\"phone_mobile\"]")).sendKeys(PhoneNo);
		// driver.findElement(By.xpath("//*[@id=\"alias\"]")).sendKeys(Address);

		driver.findElement(By.xpath("//*[@id=\"submitAccount\"]/span")).click();

		By locator1 = By.xpath("//*[@class=\"navigation_page\"]");
		if (isElementPresent(locator1)) {
			System.out.println("Tao acc thanh cong");
		} else {
			fail("tao acc khong thanh cong");

		}

	}

	@Test(priority = 3)
	public void testPro3() throws InterruptedException {
		driver.get("http://automationpractice.com/");

		// sau lan thu 1 phai thay doi email truoc khi run.
		String email = "111qq221tuanorks250298@gmail.com";
		driver.findElement(By.xpath("//*[@id=\"newsletter-input\"]")).sendKeys(email);
		driver.findElement(By.xpath("//*[@id=\"newsletter_block_left\"]/div/form/div/button")).click();
		Thread.sleep(2000);

		By letter = By
				.xpath("//*[contains(text(),'Newsletter : You have successfully subscribed to this newsletter.')]");
		if (isElementPresent(letter)) {
			System.out.println("test letter pass");
		} else {

			System.out.println("test letter down because your email is invalid or already registry");
		}
	}

	@Test(priority = 4)
	public void testPro4() throws InterruptedException {
		// contact us

		driver.get("http://automationpractice.com/");
		driver.findElement(By.xpath("//*[@id=\"contact-link\"]/a")).click();

		Thread.sleep(2000);
		String email = "tuannguyen@gmail.com";
		Select contact1 = new Select(driver.findElement(By.xpath("//*[@id=\"id_contact\"]")));
		contact1.selectByIndex(1);
		String email1 = driver.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("value");
		if (!email1.equals(email)) {
			driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(email);
		} else {
			System.out.println("email trong contact us da duoc dien tu dong " + email);
		}
		driver.findElement(By.xpath("//*[@id=\"fileUpload\"]")).sendKeys("C:\\Users\\LQA\\Downloads\\easyinfo.txt");
		driver.findElement(By.xpath("//*[@id=\"message\"]")).sendKeys("tuan");
		driver.findElement(By.xpath("//*[@id=\"submitMessage\"]/span")).click();

		By locator = By.xpath("//*[contains(text(),'Your message has been successfully sent to our team.')]");
		if (isElementPresent(locator)) {
			System.out.println("done");
		} else {
			System.out.println("mail gui khong thanh cong");
		}
	}

	@Test(priority = 5)
	public void testPro5() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).click();
		String content = "Dress";
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);

		// kiem tra xem Search duoc xoa di sau khi nhap hay chua
		String contentauto = driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).getAttribute("value");
		if (contentauto.contains("Search")) {
			System.out.println("Search is not deleted");
		} else {
			System.out.println("Search is deleted");
		}
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).clear();
		Thread.sleep(2000);

		// kiem tra xem Search co quay lai sau khi xoa content di khong
		String contentauto1 = driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).getAttribute("placeholder");
		if (contentauto1.contains("Search")) {
			System.out.println("Search is back");
		} else {
			System.out.println("Search is not back");
		}

	}

	@Test(priority = 6)
	public void testPro6() throws InterruptedException {
		// check tu goi y hien ra dung voi keyword hay khong
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).click();
		String content = "Dress";
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(2000);

		List<WebElement> list = driver.findElements(By.xpath("//*[@id=\"index\"]/div[2]/ul/li"));
		for (int i = 0; i < list.size(); i++) {
			System.out.println((list.get(i)).getText());
			if ((list.get(i)).getText().contains(content)) {
				System.out.println("goi y dua ra dung voi key word " + content + "\n");
			} else {
				System.out.println("goi y dua ra khong dung voi key word " + content + "\n");

			}
		}
	}

	@Test(priority = 7)
	public void testPro61() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).click();
		String content = "Dress";
		Thread.sleep(1000);

		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(4000);

		// click vao ket qua dau tien xo xuong
		String sug1 = driver.findElement(By.xpath("//*[@id=\"index\"]/div[2]/ul/li[1]")).getText();
		driver.findElement(By.xpath("//*[@id=\"index\"]/div[2]/ul/li[1]")).click();
		Thread.sleep(2000);

		// check xem sug1 co contains nameProduct hay khong
		String nameProduct = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/h1")).getText();
		System.out.println(sug1);
		if (sug1.contains(nameProduct)) {
			System.out.println("correct");
		} else {
			System.out.println("incorrect");
		}

		// sendKeys de hien thi suggest list
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(4000);

		// khai bao 1 suggest list

		List<WebElement> list = driver.findElements(By.xpath("//div[@class=\"ac_results\"]/ul/li"));
		System.out.println("before loop");
		for (int i = 1; i < list.size(); i++) {
			List<WebElement> list1 = driver.findElements(By.xpath("//div[@class=\"ac_results\"]/ul/li"));
			System.out.println("in loop: i=" + i);
			WebElement xWebElement = list1.get(i);
			String xWebElementGetText = xWebElement.getText();
			System.out.println(list1.get(i).getText());

			xWebElement.click();
			Thread.sleep(2000);

			String namePro = driver.findElement(By.xpath("//div[@class='pb-center-column col-xs-12 col-sm-4']//h1"))
					.getText();

			// kiem tra contains hay khong
			if (xWebElementGetText.contains(namePro)) {
				System.out.println("correct");
			} else {
				System.out.println("incorrect");
				// fail("incorrect");
			}
			driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
			Thread.sleep(3000);
		}

	}

	@Test(priority = 8)
	public void testPro62() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		String content = "dress";
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		driver.findElement(By.xpath("//*[@id=\"searchbox\"]/button")).click();
		Thread.sleep(2000);
		String result = driver.findElement(By.xpath("//*[@id=\"center_column\"]/h1/span[@class='heading-counter']"))
				.getText();
		String resultCut = result.substring(0, 1);
		System.out.println(resultCut);
		float result1 = Float.parseFloat(resultCut);
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(2000);
		List<WebElement> list = driver.findElements(By.xpath("//*[@id=\"search\"]/div[2]/ul/li"));
		int count = list.size();
		System.out.println("size = " + count);
		if (count == result1) {
			System.out.println("so luong ket qua dung voi search");
		} else {
			System.out.println("so luong ket qua sai voi search");
		}

	}

	@Test(priority = 9)
	public void testPro63() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).click();
		String content = "Dress";
		Thread.sleep(1000);

		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(4000);

		driver.findElement(By.xpath("//*[@id=\"index\"]/div[2]/ul/li[1]")).click();
		Thread.sleep(2000);
		By price = By.xpath("//*[@id=\"our_price_display\"]");
		if (isElementPresent(price)) {
			System.out.println("co xuat hien price cua san pham");
		} else {
			System.out.println("khong xuat hien price cua san pham");
		}
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
		Thread.sleep(4000);
		List<WebElement> list = driver.findElements(By.xpath("//div[@class=\"ac_results\"]/ul/li"));

		for (int i = 1; i < list.size(); i++) {
			List<WebElement> list1 = driver.findElements(By.xpath("//div[@class=\"ac_results\"]/ul/li"));
			WebElement Elm = list1.get(i);
			Elm.click();
			Thread.sleep(2000);

			if (isElementPresent(price)) {
				System.out.println("co xuat hien price cua san pham thu " + i);
			} else {
				System.out.println("khong xuat hien price cua san pham");
			}
			driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(content);
			Thread.sleep(4000);
		}
	}

	@Test(priority = 10)
	public void testPro7() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		String wrong = "dressie";
		driver.findElement(By.xpath("//*[@id=\"search_query_top\"]")).sendKeys(wrong);
		driver.findElement(By.xpath("//*[@id=\"searchbox\"]/button")).click();
		Thread.sleep(2000);
		By locator = By.xpath("//*[@id=\"center_column\"]/p");
		if (isElementPresent(locator)) {
			System.out.println("test dung");
		} else {
			System.out.println("test sai");
		}

	}

	@Test(priority = 11)
	public void testPro8() throws InterruptedException {
		// test tong cua gia san pham va thuc hien proceed den khi hoan thanh
		driver.get("http://automationpractice.com/index.php");
		// hover and add to cart 3 san pham
		Actions actions = new Actions(driver);
		WebElement Product = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[1]/div/a[1]/img"));
		actions.moveToElement(Product).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions1 = new Actions(driver);
		WebElement Product1 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[1]/div/a[1]/img"));
		actions1.moveToElement(Product1).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions2 = new Actions(driver);
		WebElement Product2 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/a[1]/img"));
		actions2.moveToElement(Product2).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span")).click();
		Thread.sleep(3000);
		// tao 1 list gia san pham cua cac san pham
		List<WebElement> list_total = driver.findElements(By.xpath("//td[@class = \"cart_total\"]"));
		float sum = 0;
		for (int i = 0; i < list_total.size(); i++) {
			// System.out.println(list_total.get(i).getText());
			String string = list_total.get(i).getText();
			String ele = string.substring(1);
			System.out.println("gia cua san pham " + i + " " + string.substring(1));
			float element = Float.parseFloat(ele);
			sum = sum + element;
		}
		// so sanh tong
		String total = driver.findElement(By.xpath("//*[@id=\"total_product\"]")).getText();
		String TotalCut = total.substring(1);
		float totalfloat = Float.parseFloat(TotalCut);
		System.out.println("tong thuc cua cac san pham " + sum + "\ntong cua webpage " + totalfloat);
		if (totalfloat == sum) {
			System.out.println("tong gia san pham duoc tinh dung");

		} else {
			System.out.println("tong gia san pham da tinh sai");
		}
		driver.findElement(By.xpath(
				"//*[@id=\"center_column\"]/p[@class='cart_navigation clearfix']/a[@title=\"Proceed to checkout\"]"))
				.click();
		Thread.sleep(2000);
		// dang nhap thong tin de tiep tuc
		String email = "tuannguyen@gmail.com";
		String pw = "12345";
		driver.findElement(By.xpath("//input[@id=\"email\"]")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id=\"passwd\"]")).sendKeys(pw);
		driver.findElement(By.xpath("//button[@id=\"SubmitLogin\"]")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//button[@name=\"processAddress\"]")).click();
		Thread.sleep(2000);
		// check checkbox
		WebElement checkbox = driver.findElement(By.xpath("//*[@id=\"cgv\"]"));
		if (!checkbox.isSelected()) {
			checkbox.click();
		}
		driver.findElement(By.xpath("//button[@name=\"processCarrier\"]")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//a[@title=\"Pay by bank wire\"]")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//button[@class=\"button btn btn-default button-medium\"]")).click();
		Thread.sleep(2000);
		// check xem mua thanh cong hay chua
		String content = driver.findElement(By.xpath("//p[@class=\"cheque-indent\"]")).getText();
		By locator = By.xpath("//p[@class=\"cheque-indent\"]");
		if (isElementPresent(locator)) {
			System.out.println("xuat hien content=>>>>" + content + "<<<<=test thanh cong");
		} else {
			System.out.println("khong xuat hien content=>>>>" + content + "<<<<=test khong thanh cong");
		}
	}

	@Test(priority = 12)
	public void testPro9() throws InterruptedException {
		driver.get("http://automationpractice.com/");
		// chon 5 san pham
		Actions actions = new Actions(driver);
		WebElement Product = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[1]/div/a[1]/img"));
		actions.moveToElement(Product).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions1 = new Actions(driver);
		WebElement Product1 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[1]/div/a[1]/img"));
		actions1.moveToElement(Product1).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions2 = new Actions(driver);
		WebElement Product2 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/a[1]/img"));
		actions2.moveToElement(Product2).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions3 = new Actions(driver);
		WebElement Product3 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[4]/div/div[1]/div/a[1]/img"));
		actions3.moveToElement(Product3).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[4]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		Actions actions4 = new Actions(driver);
		WebElement Product4 = driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[5]/div/div[1]/div/a[1]/img"));
		actions4.moveToElement(Product4).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[5]/div/div[2]/div[2]/a[1]/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")).click();
		Thread.sleep(3000);

		List<WebElement> list = driver.findElements(By.xpath("//input[@size='2']"));
		for (int i = 0; i < 1; i++) {
			WebElement input = list.get(i);
			input.clear();
			input.sendKeys("3");
		}
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span")).click();
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("tuannguyen@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"passwd\"]")).sendKeys("12345");
		driver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"center_column\"]/form/p/button")).click();
		Thread.sleep(2000);

		WebElement checkbox = driver.findElement(By.xpath("//*[@id=\"cgv\"]"));
		if (checkbox.isSelected()) {
			checkbox.click();
		}

		driver.findElement(By.xpath("//*[@id=\"form\"]/p/button")).click();
		Thread.sleep(2000);

		By locator = By.xpath("//*[@id=\"order\"]/div[2]/div/div/div/div/p");
		WebElement element = driver.findElement(By.xpath("//*[@id=\"order\"]/div[2]/div/div/div/div/p"));
		String text = element.getText();
		if (isElementPresent(locator)) {
			System.out.println("element co xuat hien va noi dung cua no la =>>>>>> " + text);
			driver.findElement(By.xpath("//*[@id=\"order\"]/div[2]/div/div/a")).click();
		} else {
			System.out.println("element doesnt exist");
		}
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"cgv\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"form\"]/p/button/span")).click();
		Thread.sleep(3000);

		/////////// tinh tong
		List<WebElement> list_total = driver.findElements(By.xpath("//td[@class = \"cart_total\"]"));
		float sum = 0;
		for (int i = 0; i < list_total.size(); i++) {
			// System.out.println(list_total.get(i).getText());
			String string = list_total.get(i).getText();
			String ele = string.substring(1);
			System.out.println("gia cua san pham " + i + " " + string.substring(1));
			float element1 = Float.parseFloat(ele);
			sum = sum + element1;
		}

		String total = driver.findElement(By.xpath("//*[@id=\"total_product\"]")).getText();
		String TotalCut = total.substring(1);
		float totalfloat = Float.parseFloat(TotalCut);
		System.out.println("tong thuc cua cac san pham " + sum + "\ntong cua webpage " + totalfloat);
		if (totalfloat == sum) {
			System.out.println("tong gia san pham duoc tinh dung");

		} else {
			System.out.println("tong gia san pham da tinh sai");
		}

		driver.findElement(By.xpath("//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"cart_navigation\"]/button/span")).click();
		By complete = By.xpath("//*[@id=\"center_column\"]/div/p/strong");
		if (isElementPresent(complete)) {
			System.out.println("thanh toan thanh cong");
		} else {
			System.out.println("fail");
		}

	}

	@Test(priority = 13)
	public void testPro10() throws InterruptedException {
		driver.get("http://automationpractice.com");
		List<WebElement> list = driver.findElements(By.xpath("//*[@id=\"homefeatured\"]/li"));
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getText());

			WebElement product = list.get(i);
			String needToBuy = list.get(i).getText();
			String sale = "20%";

			if (needToBuy.contains(sale)) {
				product.click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button/span")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span")).click();
			}
		}

	}

	@Test(priority = 14)
	public void testPro11() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/a[1]/img")).click();
		Thread.sleep(2000);
		int height = driver.findElement(By.xpath("//img[@id = \"bigpic\"]")).getSize().getHeight();
		int width = driver.findElement(By.xpath("//img[@id = \"bigpic\"]")).getSize().getWidth();

		System.out.println(height);
		System.out.println(width);
		driver.findElement(By.xpath("//img[@id = \"bigpic\"]")).click();
		Thread.sleep(2000);
		int Zoomheight = driver.findElement(By.xpath("//img[@class=\"fancybox-image\"]")).getSize().getHeight();
		int Zoomwidth = driver.findElement(By.xpath("//img[@class=\"fancybox-image\"]")).getSize().getWidth();

		System.out.println(Zoomheight);
		System.out.println(Zoomwidth);
		if ((height < Zoomheight && width < Zoomwidth) || (height < Zoomheight && width == Zoomwidth)
				|| (height == Zoomheight && width < Zoomwidth)) {
			System.out.println("da zoom");
		} else {
			fail("chua zoom");
		}
		Point locationtitle = driver.findElement(By.xpath("//span[@class = \"child\"]")).getLocation();
		System.out.println("vi tri cua title =>>>>>>(X,Y) = " + locationtitle);
		int Ytitle = locationtitle.getY();
		System.out.println(Ytitle);
		Point locationimg = driver.findElement(By.xpath("//img[@class=\"fancybox-image\"]")).getLocation();
		System.out.println("vi tri cua image =>>>>>>(X,Y) = " + locationimg);
		int Yimg = locationimg.getY();
		System.out.println(Yimg);
		if (Ytitle - Yimg > 0) {
			System.out.println("title nam duoi img");
		} else {
			fail("title nam cung vi tri voi anh hoac nam tren");
		}
		//////// Quantity = 0
		driver.findElement(By.xpath("//a[@title=\"Close\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"quantity_wanted\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"quantity_wanted\"]")).sendKeys("0");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button")).click();
		Thread.sleep(2000);
		String Error = driver.findElement(By.xpath("//p[@class=\"fancybox-error\"]")).getText();
		System.out.println("in ra " + Error);

		Thread.sleep(1000);
		if (Error.contains("Null quantity.")) {
			System.out.println("xuat hien error");
		} else {
			System.out.println("khong xuat hien =>>>> test fail");
		}
	}

	@Test(priority = 15)
	public void testPro12() throws InterruptedException {
		driver.get("http://automationpractice.com/");

		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[3]/div/div[1]/div/a[1]/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/p[7]/button[1]")).click();
		String parent = driver.getWindowHandle();
		System.out.println(parent);
		Thread.sleep(3000);

		Set<String> handles = driver.getWindowHandles();
		String twitter = "";
		for (String h : handles) {
			if (!h.equals(parent)) {
				twitter = h;
				System.out.println(h);
			}

		}
		try {
			driver.switchTo().window(twitter);
			System.out.println("Title popup = " + driver.getTitle());
			Thread.sleep(3000);
			driver.findElement(By.xpath("//input[@name='session[username_or_email]']")).sendKeys("tuan");
			driver.findElement(By.xpath("//input[@name=\"session[password]\"]")).sendKeys("nguyen");
			Thread.sleep(2000);
			driver.close();
			driver.switchTo().window(parent);

		} catch (NoSuchWindowException e) {
			System.out.println("khong the switch");

			Thread.sleep(2000);
		}
		
	}

	@Test(priority = 16)
	public void testPro13() throws InterruptedException {
		driver.get("http://automationpractice.com/index.php");
		String email = "tuannguyen@gmail.com";
		String pw = "12345";

		driver.findElement(By.xpath("//a[@class=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id=\"email\"]")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id=\"passwd\"]")).sendKeys(pw);
		driver.findElement(By.xpath("//button[@id=\"SubmitLogin\"]")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//img[@class=\"logo img-responsive\"]")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/a[1]/img")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//a[@class=\"open-comment-form\"]")).click();
		Thread.sleep(1000);

		driver.findElement(By.xpath("//input[@id=\"comment_title\"]")).sendKeys("comment dao cua Tuan");
		driver.findElement(By.xpath("//textarea[@id=\"content\"]")).sendKeys("khong them mua nua");
		driver.findElement(By.xpath("//button[@id=\"submitNewMessage\"]")).click();
		Thread.sleep(2000);

		By locator = By.xpath("//div[@class=\"fancybox-inner\"]");

		if (isElementPresent(locator)) {
			System.out.println("comment thanh cong");
		} else {
			System.out.println("comment khong thanh cong");
		}
		driver.findElement(By.xpath("//button[@class=\"button btn-default button-medium\"]")).click();
	}

	@Test(priority = 17)
	public void testPro14() throws InterruptedException {
		driver.get("https://www.garena.vn/");
	}
	@AfterMethod
	public void afterMethod() throws InterruptedException {
		Thread.sleep(1000);
		driver.close();

	}

}
